2020.06.26
项目打板
这应该是我实际意义上的第一个完整的项目
项目过程中遇到很多因为vue版本升级与自身知识储备不足而出现的bug
不过幸而网上解答还是全的 
感恩
我也写不出来什么花
就把我学到的知识点列一下吧

## 1 动态添加类的三种方式：

-   :class="starType"（starType由计算属性获得；prop变量与定量拼接；）
-   ：class="classMap[i]"（classMap是由created时创建，都是定量）
-   :class={'current':currentIndex===index}"条件类(currentIndex通过计算属性或者data获得，如果条件成立，则添加current属性)
-   css: 同级下面一层 &.

## 2 better-scroll

-   better-scroll 是什么

better-scroll 是一款重点解决移动端（已支持 PC）各种滚动场景需求的插件。

-   滚动原理

浏览器的滚动原理： 浏览器的滚动条大家都会遇到，当页面内容的高度超过视口高度的时候，会出现纵向滚动条；当页面内容的宽度超过视口宽度的时候，会出现横向滚动条。也就是当我们的视口展示不下内容的时候，会通过滚动条的方式让用户滚动屏幕看到剩余的内容。

绿色部分为 wrapper，也就是父容器，它会有固定的高度。黄色部分为 content，它是父容器的第一个子元素，它的高度会随着内容的大小而撑高。那么，当 content 的高度不超过父容器的高度，是不能滚动的，而它一旦超过了父容器的高度，我们就可以滚动内容区了。

-   调用
-   npm install better-scroll
-   import BScroll from 'better-scroll'
-   ref="rightWrapper"//滚动区域容器
-   methods: {
	_initScroll(){
		this.rightScroll=new BScroll(this.$refs.rightWrapper,{
		click:true,//可点击
		probe:3//可实时获取滚动位置
}) }}
-   created(){
this.$nextTick(()=>{
this._initScroll();
}) }
1) Element.getBoundingClientRect() 方法返回元素的大小及其相对于视口的位置。
2) this.rightScroll.scrollToElement(el, time, offsetX, offsetY, easing)
-   参数：
-   {DOM | String} el 滚动到的目标元素, 如果是字符串，则内部会尝试调用 querySelector 转换成 DOM 对象。
-   {Number} time 滚动动画执行的时长（单位 ms）
-   {Number | Boolean} offsetX 相对于目标元素的横轴偏移量，如果设置为 true，则滚到目标元素的中心位置
-   {Number | Boolean} offsetY 相对于目标元素的纵轴偏移量，如果设置为 true，则滚到目标元素的中心位置
-   {Object} easing 缓动函数，一般不建议修改，如果想修改，参考源码中的 ease.js 里的写法
-   返回值：无
-   作用：滚动到指定的目标元素。
3) this.rightScroll.refresh()
-   参数：无
-   返回值：无
-   作用：重新计算 better-scroll，当 DOM 结构发生变化的时候务必要调用确保滚动的效果正常。
4) scroll----事件
-   参数：{Object} {x, y} 滚动的实时坐标
-   触发时机：滚动过程中，具体时机取决于选项中的 [probeType]
-   this.rightScrolll.on("scroll",(pos)=>{
this.scrollY=Math.abs(Math.round(pos.y));})

## 3 Vue

.全局属性：Vue.set( target, propertyName/index, value )
-   参数：
-   {Object | Array} target
-   {string | number} propertyName/index
-   {any} value
-   返回值：设置的值。
-   用法：
向响应式对象中添加一个 property，并确保这个新 property 同样是响应式的，且触发视图更新。它必须用于向响应式对象上添加新 property，因为 Vue 无法探测普通的新增 property (比如 this.myObject.newProperty = 'hi')
注意:对象不能是 Vue 实例，或者 Vue 实例的根数据对象。
.全局属性：Vue.nextTick( [callback, context] [)](https://cn.vuejs.org/v2/api/#Vue-nextTick)
-   参数：
-   {Function} [callback]
-   {Object} [context]
-   用法：
在下次 DOM 更新循环结束之后执行延迟回调。在修改数据之后立即使用这个方法，获取更新后的 DOM。
.实例方法 / 事件:vm.$emit( eventName, […args] )
-   参数：
-   {string} eventName 触发事件
-   [...args] 参数
触发当前实例上的事件。附加参数都会传给监听器回调。【父组件； @eventName="method"】
.Vue.filter( id, [definition] )
-   参数：
-   {string} id
-   {Function} [definition]
-   用法：
注册或获取全局过滤器。

## 4 CSS

 - 实现文本多余省略号代替
{white-space:nowrap;
overflow:hidden;
text-overflow:ellipsis;}
- 清除浮动
.clearfix{
display:inline-block;
&:after{
display:block;
content:"";
height:0;
line-height;0;
clear:both;
visibility:hidden;
}}
- display:flex
-- flex属性
flex属性是flex-grow, flex-shrink 和 flex-basis的简写，默认值为0 1 auto。后两个属性可选。
-- flex-grow属性
flex-grow属性定义项目的放大比例，默认为0，即如果存在剩余空间，也不放大。
-- flex-shrink属性
flex-shrink属性定义了项目的缩小比例，默认为1，即如果空间不足，该项目将缩小。

- flex-basis属性
flex-basis属性定义了在分配多余空间之前，项目占据的主轴空间（main size）。浏览器根据这个属性，计算主轴是否有多余空间。它的默认值为auto，即项目的本来大小。
.item { flex-basis:  <length>  |  xpx;  /* default auto */
width:xpx; }
它可以设为跟width或height属性一样的值（比如350px），则项目将占据固定空间。
-  fontsize:0;消除多余空格；
- 盒子模糊效果{
filter:blur(10px);
//当前层模糊 若是设置底层模糊直接：backdrop-filter:blur(10px)
z-index:-1;
width:
height:}
- transition
<transition name="fade">  <div class="show">hello</p></transition>
css：
 .show{
&.fade-enter-active,& .fade-leave-active 
{ transition: opacity .5s;//设定切换时间 } 
&.fade-enter,& .fade-leave-to /* &.fade-leave-active below version 2.1.8 */ 
{ opacity: 0;//进入透明度为0 } }
transform:translate3d(x,y,z);//沿着各个方向的位移
JavaScript 钩子
可以在 attribute 中声明 JavaScript 钩子
<transition v-on:before-enter="beforeEnter" 
v-on:enter="enter" v-on:after-enter="afterEnter" v-on:enter-cancelled="enterCancelled" v-on:before-leave="beforeLeave" v-on:leave="leave" v-on:after-leave="afterLeave" v-on:leave-cancelled="leaveCancelled"></transition>
 
