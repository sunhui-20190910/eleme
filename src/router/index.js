import Vue from 'vue';
import Router from 'vue-router';
import goods from '../components/goods/goods.vue';
import ratings from '../components/ratings/ratings.vue';
import seller from '../components/seller/seller.vue';
/*
import App from '../App';
*/

Vue.use(Router);
 /* eslint-disable */


export default new Router({
  routes: [
    /*{
      path: '/',
      name: 'App',
      component: App
    },*/{
      path: '/',
      // name: 'ratings',
      // component: ratings
      name: 'goods',
      component: goods
    },{
      path: '/ratings',
      name: 'ratings',
      component: ratings
    },{
      path: '/seller',
      name: 'seller',
      component: seller
    }
  ]
})
