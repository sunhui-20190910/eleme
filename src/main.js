// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import router from './router';
import App from './App';
import vueSource from '../node_modules/vue-source';
import axios from '../node_modules/axios';
import '../static/css/reset.css';
import '../src/common/iconfont/iconfont.css';
import './common/stylus/index.styl';
Vue.use(vueSource);
Vue.prototype.axios = axios;
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  vueSource,
  components: { App },
  template: '<App/>'
});
